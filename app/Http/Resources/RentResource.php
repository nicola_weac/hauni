<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class RentResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'booking_id' => $this->booking_id,
            'name' => $this->name,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'date' => $this->date
        ];
    }
}
