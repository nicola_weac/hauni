<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ParkingResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'date' => $this->date,
            'parkingStatus' => $this->parkingStatus,
            'additional' => $this->addition,
            'shared' => $this->shared,
            'rented' => $this->rented,
            'free' => $this->status == 1 ? false : true,
            'created_at' => (String)$this->created_at
        ];
    }
}
