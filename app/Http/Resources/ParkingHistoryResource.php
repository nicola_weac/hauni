<?php

namespace App\Http\Resources;

use App\Models\ParkingSpacesRent;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Support\Facades\Auth;

class ParkingHistoryResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->parking->id,
            'rent_id' => $this->id,
            'name' => $this->parking->name,
            'lat' => $this->parking->lat,
            'lng' => $this->parking->lng,
            'date' => $this->date,
            'complete' => $this->date > Carbon::now()->format('Y-m-d') ? false : true,
            'cancelled' => $this->cancelled == 1 ? true : false,
            'status' => $this->ownParking(),
        ];
    }

    private function ownParking(){
        $authUser = Auth::guard('api')->user();

        if ($authUser->ownParking()->exists()){
            if ($this->parking_space_id == $authUser->ownParking->id){
                return 'shared';
            }else{
                return 'rented';
            }
        }else{
            return 'rented';
        }
    }
}
