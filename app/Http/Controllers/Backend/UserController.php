<?php

namespace App\Http\Controllers\Backend;

use App\Models\Members;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    public function delete($userid){

        /** @var Members $authUser */
        $authUser = Auth::guard('backend')->user();

        if ($authUser->hasAnyRoles('ss')){
            $User = User::find($userid);

            if ($User){
                $User->delete();
            }
        }else{
            return response()
                ->view('backend.errorSites.403', ['routeBack' => 'admin.users'], 403)
                ->header('Content-Type', 'text/html');
        }

        return Redirect::route('admin.users');
    }

    public function index(Request $request){

        $User = User::where('id', '>', 0);

        if ($request->has('filter')){
            if ($request->input('filter') == 'intern'){
                $User->where('name', null)->orWhere('name','');
            }elseif ($request->input('filter') == 'extern'){
                $User->where('email', null)->orWhere('email', '');
            }
        }

        return view('backend.user.index')
            ->with('User', $User->paginate(15));
    }
}
