<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function deleteUser(Request $request){

        $v = Validator::make($request->all(), [
            'password' => 'required'
        ]);

        if ($v->fails()){
            return Response::json([
                'message' => $v->messages(),
            ], 404);
        }else{

            $authUser = Auth::guard('api')->user();

            if (Hash::check($request->input('password'), $authUser->password)){

                $authUser->userParkingRent()->delete();
                $authUser->delete();

            }else{
                return Response::json([
                    'message' => 'Das eingegebene Passwort stimmt nicht mit dem deines Accounts überein.'
                ], 404);
            }
        }

        return Response::json([
            'message' => 'Success'
        ], 200);
    }

    public function changePassword(Request $request){

        $v = Validator::make($request->all(), [
            'new_password' => 'required|string|confirmed',
            'old_password' => 'required|string'
        ]);

        if($v->fails()){
            return Response::json([
                'message' => $v->messages()
            ], 404);
        }else{

            /** @var User $authUser */
            $authUser = Auth::guard('api')->user();

            if (!$authUser->isIntern()){
                return Response::json([
                    'message' => 'Nur interne User können ihr Passwort ändern.'
                ], 404);
            }

            if (Hash::check($request->input('old_password'), $authUser->password)){

                $User = User::find($authUser->id);
                $User->password = bcrypt($request->input('new_password'));
                $User->save();
            }else{
                return Response::json([
                    'message' => 'Password wrong.'
                ], 404);
            }

        }

        return Response::json([
            'message' => 'Password has been updated.'
        ],200);
    }
}