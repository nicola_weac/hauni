<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ContentResource;
use App\Models\Content;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContentController extends Controller
{
    public function showOne($identifier){

        $Content = Content::where('identifier', $identifier)->first();


        return new ContentResource($Content);
    }
}
