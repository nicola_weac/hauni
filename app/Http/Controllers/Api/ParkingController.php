<?php

namespace App\Http\Controllers\Api;

use App\Classes\Parking;
use App\Http\Resources\ParkingHistoryResource;
use App\Http\Resources\ParkingResource;
use App\Http\Resources\RentResource;
use App\Models\ParkingSpaceReport;
use App\Models\ParkingSpaces;
use App\Models\ParkingSpacesFree;
use App\Models\ParkingSpacesRent;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class ParkingController extends Controller
{
    public function reportParkingSpace(Request $request){

        $v = Validator::make($request->all(), [
            'parking_id' => 'required'
        ]);

        if ($v->fails()){
            return Response::json([
                'message' => $v->messages(),
            ], 404);
        }else{

            ParkingSpaceReport::create([
                'parking_space_id' => (int)$request->input('parking_id'),
                'reason' => $request->input('type'),
                'message' => $request->input('text'),
                'read_at' => false,
            ]);
        }
        return Response::json([
            'message' => 'Success'
        ], 200);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function showFreeSpaces(Request $request){

        $Parking = new Parking();
        $free = $Parking->searchParkingSpaces($request->input('fromDate'), $request->input('toDate'));

        return $free;
    }

    public function loadParkingSpace($parkingid){

        $Parking = ParkingSpaces::find($parkingid);

        return new ParkingResource($Parking);
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function rentParkingSpace(Request $request){

        $parking = new Parking();

        $data = explode(',', $request->input('bookingids'));

        $parking->rentParkingSpace($data);

        return Response::json([
            'message' => 'Success'
        ], 200);
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function parkingHistory(){

        /** @var User $authUser */
        $authUser = Auth::guard('api')->user();

        /** @var ParkingSpacesRent $history */
        $history = ParkingSpacesRent::where('user_id', $authUser->id)->where('cancelled', 0)->where('date', '<', Carbon::now()->format('Y-m-d'))->orderBy('date', 'DESC')->get();

        if ($authUser->ownParking()->exists()){
            $history2 = ParkingSpacesRent::where('parking_space_id', $authUser->ownParking->id)->where('date', '<', Carbon::now()->format('Y-m-d'))->get();

            $history = $history->concat($history2);
        }

        foreach ($history as $key => $value){

            if ($value->date < Carbon::now()->format('Y-m-d')){
                $value->complete = true;
            }else{
                $value->complete = false;
            }
        }

        return ParkingHistoryResource::collection($history);
    }

    /**
     * @return ParkingResource
     */
    public function showTodaysParkingSpace(){

        /** @var User $authUser */
        $authUser = Auth::guard('api')->user();
        $today = $authUser->parking()->where('cancelled', 0)->where('date', Carbon::now()->format('Y-m-d'))->first();

        if ($today == null){
            //check if own ParkingSpace
            if ($authUser->ownParking == null){
                $today = new ParkingSpaces();
                $today->parkingStatus = 'none';
                $today->addition = ParkingSpacesRent::where('user_id', $authUser->id)->where('cancelled', 0)->where('date', '>=', Carbon::now()->format('Y-m-d'))->count();

                return Response::json(
                    new ParkingResource($today)
                ,200);
            }
            $today = $authUser->ownParking;
            $today->parkingStatus = 'own';
        }else{
            $today->parkingStatus = 'rent';
        }
        if ($authUser->ownParking()->exists()){
            $today->addition = ParkingSpacesRent::where('parking_space_id', $authUser->ownParking->id)->where('cancelled', 0)->where('date', '>=', Carbon::now()->format('Y-m-d'))->count() - 1;
        }else{
            $today->addition = ParkingSpacesRent::where('user_id', $authUser->id)->where('cancelled', 0)->where('date', '>=', Carbon::now()->format('Y-m-d'))->count() - 1;
        }

        $today->date = Carbon::now()->format('Y-m-d');

        if ($authUser->ownParking()->exists()){
            if (ParkingSpacesRent::where('date', Carbon::now()->format('Y-m-d'))->where('parking_space_id', $authUser->ownParking->id)->exists()){

                $today->rented = true;

            }else{
                $today->rented = false;
            }

            if (ParkingSpacesFree::where('date', Carbon::now()->format('Y-m-d'))->where('parking_space_id', $authUser->ownParking->id)->exists()){
                $today->shared = true;
            }else{
                $today->shared = false;
            }
        }

        return Response::json(
            new ParkingResource($today)
        , 200);
    }

    public function cancelOfferedParkingSpace(Request $request){

        $data = explode(',', $request->input('shareids'));

        foreach ($data as $key => $value){

            $parking = ParkingSpacesFree::where('status', 0)->where('id', $value)->first();

            if ($parking){
                $parking->delete();
            }else{
                return Response::json([
                    'message' => 'Fail',
                ], 404);
            }
        }
        return Response::json([
            'message' => 'Success'
        ], 200);

    }

    /**
     * @param Request $request
     * @return Response
     */
    public function offerOwnParkingSpace(Request $request){

        $v = Validator::make($request->all(), [
            'fromDate' => 'required',
            'toDate' => 'required'
        ]);

        if ($v->fails()){
            return Response::json([
                'message' => $v->messages(),
            ]);
        }else{

            $parking = new Parking();
            $return = $parking->offerOwnParkingSpace($request->input('fromDate'), $request->input('toDate'));

            return $return;
        }
    }

    public function cancelRentedParkingSpace(Request $request){

        $data = explode(',', $request->input('rentids'));

        foreach ($data as $key => $value){

            $parking = ParkingSpacesRent::where('id', $value)->where('cancelled', 0)->first();

            if ($parking){
                $date = $parking->date;
                $parkingSpaceId = $parking->parking_space_id;
                if ($date < Carbon::now()->format('Y-m-d')){
                    return Response::json([
                        'message' => 'You cannot cancel this Booking anymore.'
                    ], 404);
                }else{
                    $parking->update(['cancelled' => true]);
                    ParkingSpacesFree::create([
                        'parking_space_id' => $parkingSpaceId,
                        'date' => $date,
                    ]);
                }
            }else{
                return Response::json([
                    'message' => 'Already cancelled.'
                ], 404);
            }

        }

        return Response::json([
            'message' => 'Success',
        ], 200);
    }

    public function showOpenBookings(){

        /** @var User $authUser */
        $authUser = Auth::guard('api')->user();

        if ($authUser->hasAnyRoles('owner')){

            $bookings = ParkingSpacesRent::where('parking_space_id', $authUser->ownParking->id)->where('date', '>=', Carbon::now()->format('Y-m-d'))->where('cancelled', 0)->get();

        }elseif($authUser->hasAnyRoles('renter')){

            $bookings = ParkingSpacesRent::where('user_id', $authUser->id)->where('date', '>=', Carbon::now()->format('Y-m-d'))->where('cancelled', 0)->orderBy('date', 'ASC')->get();

        }

        return ParkingHistoryResource::collection($bookings);
    }

    public function showOfferedParkingSpaceDates(){

        /** @var User $authUser */
        $authUser = Auth::guard('api')->user();

        $parking = ParkingSpacesFree::where('parking_space_id', $authUser->ownParking->id)->orderBy('date', 'ASC')->get();

        return ParkingResource::collection($parking);
    }
}