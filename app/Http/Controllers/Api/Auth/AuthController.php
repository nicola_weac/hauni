<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\EmailWhitelist;
use App\Models\ParkingStatus;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

class AuthController extends Controller
{
    private $client;

    public function __construct()
    {
        $this->client = DB::table('oauth_clients')->where('id', 2)->first();
    }

    public function login(Request $request){

        $v = Validator::make($request->all(), [
            'username' => 'required',
            'password' => 'required'
        ]);

        if ($v->fails()){
            return Response::json([
                'message' => $v->messages(),
            ], 404);
        }else{

            $request->request->add([
                'username'  =>  $request->username,
                'password' => $request->password,
                'grant_type' => 'password',
                'client_id' => $this->client->id,
                'client_secret' => $this->client->secret,
                'scope' => '*'
            ]);

            $proxy = Request::create(
                'api/oauth/token',
                'POST'
            );

            $Response = Route::dispatch($proxy);

            if($Response->getStatusCode() != 200) {
                $jsonData = \json_decode($Response->getContent(), true);
                return Response::json([
                    'status_code' => $Response->getStatusCode(),
                    'message' => $jsonData['message'],
                    'username' => $request->username,
                ], $Response->getStatusCode());
            }

            $jsonData = \json_decode($Response->getContent(), true);
            $arr = [
                'username' => $request->input('username'),
            ];
            $data = array_merge($jsonData, $arr);
        }

        return Response::json($data, $Response->getStatusCode());
    }


    public function register(Request $request){

        if (!$this->checkWhitelist($request->input('email'))){
            return Response::json([
                'message' => 'Not Whitelisted.'
            ], 404);
        }

        $validator = [
            //'name'      =>      'required|min:4|max:20|string',
            'email'     =>      'required|email|unique:users,email',
            'password'  =>      'required|confirmed|min:6|max:255|string',
        ];

        $v = Validator::make($request->all(), $validator);

        if($v->fails()){

            return response()->json([
                'status_code'   =>      404,
                'messages'      =>      $v->messages(),
            ], 404);
        }else{

            $User = User::create([
                'email'         =>          $request->input('email'),
                'password'      =>          bcrypt($request->input('password')),
            ]);

            $status = $this->checkWhitelistForParking($User->email);

            ParkingStatus::create([
                'user_id' => $User->id,
                'status' => $status,
            ]);

            $request->request->add([
                'username' => $request->email,
                'email'     => $request->email,
                'password' => $request->password,
                'grant_type' => 'password',
                'client_id' => $this->client->id,
                'client_secret' => $this->client->secret,
                'scope' => '*'
            ]);

            $proxy = Request::create(
                'api/oauth/token',
                'POST'
            );

            $Response = Route::dispatch($proxy);
            $jsonData = \json_decode($Response->getContent(), true);
            $arr = [
                'email' => $request->input('email'),
            ];
            $data = array_merge($jsonData, $arr);

            //Mail::to($User->email)->send(new RegisterConfirmation($User));
            event($User);

            return Response::json($data, $Response->getStatusCode());
        }
    }

    private function checkWhitelist($email){

        if (EmailWhitelist::where('email', $email)->exists()){
            return true;
        }
        return false;
    }

    /**
     * @param string $email
     * @return string
     */
    private function checkWhitelistForParking($email){

        $check = EmailWhitelist::where('email', $email)->first();
        if ($check->parking_space_name == null){
            return 'none';
        }else{
            return 'own';
        }
    }
}
