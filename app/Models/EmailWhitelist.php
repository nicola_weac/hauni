<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailWhitelist extends Model
{
    protected $table = 'email_whitelist';

    protected $fillable = [
        'email', 'parking_space_name', 'registered'
    ];
}
