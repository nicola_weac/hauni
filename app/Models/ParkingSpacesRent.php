<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParkingSpacesRent extends Model
{
    protected $table = 'user_parking_space_rent';

    protected $fillable = [
        'user_id', 'parking_space_rent', 'cancelled', 'date', 'parking_space_id'
    ];

    protected $casts = [
        'date' => 'Y-m-d'
    ];

    public function parking(){
        return $this->belongsTo(ParkingSpaces::class, 'parking_space_id', 'id');
    }
}
