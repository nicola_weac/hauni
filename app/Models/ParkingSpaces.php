<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ParkingSpaces
 * @package App\Models
 *
 * @property integer user_id
 * @property string name
 * @property double lat
 * @property double lng
 * @property mixed created_at
 * @property mixed updated_at
 *
 */
class ParkingSpaces extends Model
{
    protected $table = 'parking_spaces';

    protected $fillable = [
        'user_id', 'name', 'lat', 'lng'
    ];

    protected $dates = [
        'created_at', 'updated_at'
    ];

    public function rent(){
        return $this->hasMany(ParkingSpacesRent::class, 'user_id', 'id');
    }
}
