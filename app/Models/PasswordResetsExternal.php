<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordResetsExternal extends Model
{
    protected $table = 'password_resets_external';

    protected $fillable = [
        'user_id', 'name', 'status'
    ];

    protected $dates = [
        'created_at'
    ];
}
