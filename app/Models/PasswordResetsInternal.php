<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordResetsInternal extends Model
{
    protected $table = 'password_resets_internal';

    protected $fillable = [
        'user_id', 'email', 'token',
    ];

    protected $dates = [
        'created_at'
    ];
}
