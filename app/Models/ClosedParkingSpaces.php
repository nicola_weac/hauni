<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClosedParkingSpaces extends Model
{
    protected $table = 'closed_parking_spaces';

    protected $fillable = [
        'parking_space_id', 'reason', 'from_time', 'to_time'
    ];

    protected $dates = [
        'from_time', 'to_time', 'created_at', 'updated_at'
    ];
}
