<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Auth;

class Members extends Auth
{
    protected $table = 'members';

    protected $fillable = [
        'name', 'email', 'password'
    ];

    protected $hidden = [
        'password', 'remember_token'
    ];

    public function roles(){
        return $this->belongsToMany(Roles::class, 'member_roles', 'user_id', 'role_id');
    }

    public function hasAnyRoles($roles){

        if (is_array($roles)){
            return $this->hasRoles($roles);
        }else{
            return $this->hasRole($roles);
        }

    }

    private function hasRole($role){
        return $this->roles()->where('slug', $role)->exists();
    }

    private function hasRoles($roles){

        foreach ($roles as $key => $value){
            if($this->roles()->where('slug', $value)->exists()){
                return true;
            }
        }
        return false;
    }
}
