<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ParkingSpacesFree extends Model
{
    protected $table = 'parking_spaces_free';

    protected $fillable = [
        'parking_space_id', 'date', 'status'
    ];

    protected $casts = [
        'date' => 'Y-m-d'
    ];

    public function info(){
        return $this->belongsTo(ParkingSpaces::class, 'parking_space_id', 'id');
    }

}
