<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MemberRoles extends Model
{
    protected $table = 'member_roles';

    protected $fillable = [
        'user_id', 'role_id'
    ];
}
