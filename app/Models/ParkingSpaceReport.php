<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ParkingSpaceReport extends Model
{
    protected $table = 'parking_space_reports';

    protected $fillable = [
        'reason', 'message', 'read_at', 'parking_space_id'
    ];

    protected $dates = [
        'created_at', 'updated_at', 'read_at'
    ];
}
