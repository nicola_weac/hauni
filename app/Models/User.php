<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function findForPassport($identifier) {
        return $this->orWhere('email', $identifier)->orWhere('name', $identifier)->first();
    }

    public function parking(){
        return $this->hasManyThrough(
            ParkingSpaces::class,
            ParkingSpacesRent::class,
            'user_id',
            'id',
            'id',
            'parking_space_id');
    }

    public function userParkingRent(){
        return $this->hasMany(ParkingSpacesRent::class, 'user_id', 'id')
            ->where('cancelled', false);
    }

    public function ownParking(){
        return $this->hasOne(ParkingSpaces::class, 'user_id', 'id');
    }

    public function roles(){
        return $this->belongsToMany(Roles::class, 'user_roles', 'user_id', 'role_id');
    }

    public function hasAnyRoles($roles){

        if (is_array($roles)){
            return $this->hasRoles($roles);
        }else{
            return $this->hasRole($roles);
        }

    }

    private function hasRole($role){
        return $this->roles()->where('slug', $role)->exists();
    }

    private function hasRoles($roles){

        foreach ($roles as $key => $value){
            if($this->roles()->where('slug', $value)->exists()){
                return true;
            }
        }
        return false;
    }

    public function isIntern(){
        return $this->attributes['email'] == null ? false : true;
    }

    public function isExtern(){
        return $this->attributes['name'] == null ? false : true;
    }
}
