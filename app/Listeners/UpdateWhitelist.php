<?php

namespace App\Listeners;

use App\Events\IsRegistered;
use App\Models\EmailWhitelist;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateWhitelist
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  IsRegistered  $event
     * @return void
     */
    public function handle(IsRegistered $event)
    {
        EmailWhitelist::where('email', $event->user->email)->update(['registered' => 1]);
    }
}
