<?php
/**
 * Created by PhpStorm.
 * User: bendixnicola
 * Date: 27.07.18
 * Time: 14:39
 */

namespace App\Classes;

use App\Http\Resources\RentResource;
use App\Models\ParkingSpaces;
use App\Models\ParkingSpacesFree;
use App\Models\ParkingSpacesRent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

/**
 * Class Parking
 * @package App\Classes
 */
class Parking
{
    private $fromDate;
    private $toDate;
    private $user;

    /**
     * Parking constructor.
     * @param $fromDate
     * @param $toDate
     */
    public function __construct()
    {
        $this->user = Auth::guard('api')->user();
    }

    /**
     * @todo Search for Spaces
     * @param mixed $fromDate
     * @param mixed $toDate
     * @return array ParkingSpaces
     */
    public function searchParkingSpaces($fromDate, $toDate){

        $this->fromDate = $fromDate;
        $this->toDate = $toDate;

        $dates = $this->returnDates();

        for ($i = 0; $i < count($dates); $i++){

            $parkingSpaces = ParkingSpacesFree::with('info')->where('date', $dates[$i])->inRandomOrder()->first();
            if ($parkingSpaces != null){
                $parkingSpaces->info->date = $dates[$i];
                $resourceContent = $parkingSpaces->info;
                $resourceContent->booking_id = $parkingSpaces->id;
                $parkingSpaces = new RentResource($resourceContent);
            }else{
                $fake = new ParkingSpaces();
                $fake->user_id = null;
                $fake->name = null;
                $fake->lat = null;
                $fake->lng = null;
                $fake->date = $dates[$i];

                $parkingSpaces = new RentResource($fake);
            }
            $freeParkingSpaces[] = $parkingSpaces;
        }
        return $freeParkingSpaces;
    }

    /**
     * @param array of ints $parkingids
     */
    public function rentParkingSpace($bookingids){

        foreach ($bookingids as $key => $value){

            if ($this->checkIfParkingSpaceFree($value)){

                $parkingspacefree = ParkingSpacesFree::find($value);
                $this->removeParkingSpaceFromFree($value);
                $this->makeParkingSpaceRented($parkingspacefree->parking_space_id, $parkingspacefree->date);
            }
        }

        return;
    }

    /**
     * @return mixed array
     */
    public function returnDates(){

        $numDays = abs(strtotime($this->toDate) - strtotime($this->fromDate))/60/60/24;

        $dates[] = $this->fromDate;
        $fromDate = $this->fromDate;
        for ($i = 1; $i <= $numDays; $i++) {

            $dates[] = date('Y-m-d', strtotime('+1 day' . $fromDate));
            $fromDate = end($dates);
        }

        return $dates;
    }

    /**
     * @param int $parkingid
     * @return bool
     */
    private function checkIfParkingSpaceFree($bookingid){

        if (!ParkingSpacesFree::where('id', $bookingid)->exists()){
            return false;
        }
        return true;
    }

    /**
     * @param $parkingid
     */
    private function removeParkingSpaceFromFree($bookingid){

        ParkingSpacesFree::where('id', $bookingid)->update([
            'status' => 1,
        ]);

        return;
    }

    /**
     * @param $parkingid
     * @param $date
     * @return bool
     */
    private function makeParkingSpaceRented($parkingid, $date){

        $parking = ParkingSpacesRent::create([
            'user_id' => $this->user->id,
            'parking_space_id' => $parkingid,
            'date' => $date,
        ]);

        if (!$parking){
            return false;
        }

        return true;
    }

    /**
     * @param $fromDate
     * @param $toDate
     * @return Response
     */
    public function offerOwnParkingSpace($fromDate, $toDate){

        if ($this->user->ownParking() == null){

            return Response::json([
                'message' => 'You dont own a Parking Space',
            ], 404);
        }

        $this->fromDate = $fromDate;
        $this->toDate = $toDate;

        $dates = $this->returnDates();

        if (is_array($dates)){
            foreach ($dates as $key => $value){
                if (ParkingSpacesFree::where('parking_space_id', $this->user->ownParking->id)->where('date', $value)->first() != null){
                    continue;
                }
                ParkingSpacesFree::create([
                    'parking_space_id' => $this->user->ownParking->id,
                    'date' => $value,
                ]);
            }
        }

        return Response::json([
            'message' => 'Success',
        ], 200);
    }
}