<?php

use Illuminate\Support\Facades\Route;

Route::namespace('\Auth')->group(function (){
    Route::get('/', 'LoginController@showLoginForm')->name('admin.login');
    Route::get('/login', 'LoginController@showLoginForm')->name('admin.login');
    Route::post('/login', 'LoginController@login')->name('admin.login');
    Route::get('/logout', 'LoginController@logout')->name('admin.logout');
});

Route::middleware('auth:backend')->group(function (){
    Route::get('dashboard', 'DashboardController@index')->name('admin.dashboard');

    Route::get('users', 'UserController@index')->name('admin.users');
    Route::get('users/{userid}/delete', 'UserController@delete')->name('admin.users.delete');
});
