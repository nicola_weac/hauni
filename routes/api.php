<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['prefix' => '/auth', 'namespace' => '\Auth'], function (){
    Route::post('/register', 'AuthController@register')->name('api.register');
    Route::post('/login', 'AuthController@login')->name('api.login');
});

Route::group(['middleware' => 'auth:api'], function (){

    Route::get('parking/free', 'ParkingController@showFreeSpaces')->name('api.parking.free');
    Route::get('parking/free/{parking_id}', 'ParkingController@loadParkingSpace')->name('api.parking.load');
    Route::get('parking/history', 'ParkingController@parkingHistory')->name('api.parking.history');
    Route::get('parking/today', 'ParkingController@showTodaysParkingSpace')->name('api.parking.today');

    Route::post('parking/report', 'ParkingController@reportParkingSpace')->name('api.parking.report');

    Route::get('parking/bookings/open', 'ParkingController@showOpenBookings')->name('api.parking.bookings.open');

    Route::post('parking/rent', 'ParkingController@rentParkingSpace')->name('api.parking.rent');

    Route::get('parking/status', 'ParkingController@returnParkingStatus')->name('api.parking.status');

    Route::post('parking/own/offer', 'ParkingController@offerOwnParkingSpace')->name('api.parking.own.offerd');
    Route::get('parking/own/offered', 'ParkingController@showOfferedParkingSpaceDates')->name('api.parking.own.offered');
    Route::get('parking/offer/cancel', 'ParkingController@cancelOfferedParkingSpace')->name('api.parking.cancel.offered');
    Route::get('parking/rented/cancel', 'ParkingController@cancelRentedParkingSpace')->name('api.parking.cancel.rented');

    Route::delete('user/delete', 'UserController@deleteUser')->name('api.user.delete');
    Route::post('user/changepassword', 'UserController@changePassword')->name('api.user.change.password');
});

Route::get('content/{identifier}', 'ContentController@showOne')->name('api.content');