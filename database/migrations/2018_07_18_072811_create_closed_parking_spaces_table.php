<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClosedParkingSpacesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('closed_parking_spaces', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('parking_space_id');
            $table->string('reason');
            $table->timestamp('from_time')->nullable();
            $table->timestamp('to_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('closed_parking_spaces');
    }
}
