 @extends('backend.layout.app')

@section('content')

    <!-- Hero -->
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">User</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Index</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- END Hero -->

    <!-- Page Content -->
    <div class="content">
        <!-- Dynamic Table Full -->
        <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default">
                <h3 class="block-title">User <small>alle</small></h3>
                <div class="pull-right">
                    <a href="" class="btn btn-hero-primary btn-hero-sm" data-toggle="click-ripple">
                        <i class="fa fa-plus"> </i>
                        Erstellen
                    </a>
                </div>
            </div>
            <div class="block-content block-content-full">
                <div class="row">
                    <div class="form-inline col-md-12">
                        <form action="{{ route('admin.users') }}" method="GET" class="col-md-6">
                            <input type="hidden" name="filter" value="intern">
                            <button type="submit" class="btn btn-hero-info col-md-12">
                                Nur Interne User
                            </button>
                        </form>
                        <form action="{{ route('admin.users') }}" method="GET" class="col-md-6">
                            <input type="hidden" name="filter" value="extern">
                            <button type="submit" class="btn btn-hero-info col-md-12">
                                Nur Externe User
                            </button>
                        </form>
                    </div>
                </div>
                <br>
                <!-- DataTables init on table by adding .js-dataTable-full class, functionality initialized in js/pages/be_tables_datatables.js -->
                @if($User->count() > 0)
                <table class="table table-vcenter">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>E-Mail</th>
                        <th>Intern/Extern</th>
                        <th>Registriert am:</th>
                        <th>Aktionen</th>
                    </tr>
                    @foreach($User as $key => $value)
                        <tr>
                            <td>{{ $value->id }}</td>
                            <td>{{ $value->name }}</td>
                            <td>{{ $value->email }}</td>
                            <td>{{ $value->email == null ? 'Extern' : 'Intern' }}</td>
                            <td>{{ $value->created_at }}</td>
                            <td>
                                <div class="btn-group" aria-label="Horizontal Primary" role="group">
                                    @if($value->email == null)
                                    <a href="" class="btn btn-hero-warning">
                                        <i class="fa fa-key"> </i>
                                        Reset Password
                                    </a>
                                    @endif
                                    <a href="{{ route('admin.users.delete', $value->id) }}" class="btn btn-hero-danger">
                                        <i class="fa fa-trash"> </i>
                                        Delete
                                    </a>
                                </div>
                            </td>
                        </tr>
                    @endforeach

                </table>
                @else
                    <p>Keine Daten gefunden.</p>
                @endif
                {{ $User->links() }}
            </div>
        </div>
        <!-- END Dynamic Table Simple -->
    </div>
    <!-- END Page Content -->

@endsection