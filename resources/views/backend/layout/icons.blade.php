<!-- Icons -->
<!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
<link rel="shortcut icon" href="{{ asset('images/icons/favicon.png') }}">
<link rel="icon" type="image/png" sizes="192x192" href="{{ asset('images/icons/favicon.png') }}">
<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('images/icons/favicon.png') }}">
<!-- END Icons -->